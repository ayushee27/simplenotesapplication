class AddUseridToNotes < ActiveRecord::Migration[5.2]
  def change
    add_column :notes, :userid, :integer
  end
end
